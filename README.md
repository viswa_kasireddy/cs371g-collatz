# CS371g: Generic Programming Collatz Repo

* Name: (your Full Name) Viswa Kasireddy

* EID: (your EID) vrk352

* GitLab ID: (your GitLab ID) viswa_kasireddy

* HackerRank ID: (your HackerRank ID) viswa_kasireddy

* Git SHA: (most recent Git SHA, final change to your repo will change this, that's ok) : 3381fdf474dac6b0e323e102c341a5b2794b02e0

* GitLab Pipelines: (link to your GitLab CI Pipeline) https://gitlab.com/viswa_kasireddy/cs371g-collatz/-/pipelines

* Estimated completion time: (estimated time in hours, int or float) 15

* Actual completion time: (actual time in hours, int or float) 17

* Comments: (any additional comments you have)
