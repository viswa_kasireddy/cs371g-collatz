// ---------
// Collatz.h
// ---------

#ifndef Collatz_h
#define Collatz_h

// --------
// includes
// --------

#include <iostream> // istream, ostream
#include <iterator> // istream_iterator
#include <tuple>    // tuple
#include <utility>  // pair

using namespace std;

// ------------
// cycle_length2
// ------------

/**
 * takes an integer
 * computes the cycle length
 * base code is the implementation of cycle length from class
 * uses the optimization from the quiz to divide odd numbers by 2
 * uses a lazy cache
 */

int cycle_length2 (int n);
// ------------
// collatz_read
// ------------

/**
 * read two ints
 * @param an istream_iterator
 * @return a pair of ints
 */
pair<int, int> collatz_read (istream_iterator<int>&);

// ------------
// collatz_eval
// ------------

/**
 * @param a pair of ints
 * @return a tuple of three ints
 */
tuple<int, int, int> collatz_eval (const pair<int, int>&);

// -------------
// collatz_print
// -------------

/**
 * print three ints
 * loops through inputted range with cycle_length2
 * uses a lazy cache look up
 * uses the optimization from the quiz to cut ranges in half if applicable 
 * @param an ostream
 * @param a tuple of three ints
 */
void collatz_print (ostream&, const tuple<int, int, int>&);

// -------------
// collatz_solve
// -------------

/**
 * @param an istream
 * @param an ostream
 */
void collatz_solve (istream&, ostream&);

#endif // Collatz_h
