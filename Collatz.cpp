// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <iterator> // istream_iterator
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "Collatz.hpp"

using namespace std;
long cache[1000000] = {0};
//meta cache, each index is 1000 values
int metaCache[160] = {179, 182, 217, 238, 215, 236, 262, 252, 247, 260, 268, 250, 263, 276, 271, 271, 266, 279, 261, 274, 256, 269, 269, 282, 264, 264, 308, 259, 259, 272, 272, 285, 267, 267, 311, 324, 249, 306, 244, 306, 288, 257, 288, 270, 270, 314, 283, 314, 296, 296, 278, 309, 340, 322, 260, 260, 322, 304, 273, 304, 335, 317, 286, 330, 299, 268, 268, 312, 312, 299, 312, 325, 263, 294, 325, 307, 307, 351, 338, 307, 320, 320, 320, 289, 320, 302, 302, 333, 333, 315, 315, 333, 315, 284, 315, 328, 297, 297, 284, 328, 341, 310, 310, 248, 310, 341, 354, 292, 279, 310, 292, 323, 323, 292, 305, 349, 305, 305, 336, 305, 318, 336, 318, 331, 287, 318, 331, 287, 331, 344, 331, 300, 331, 313, 300, 344, 313, 331, 313, 313, 344, 326, 375, 282, 326, 295, 357, 295, 326, 326, 370, 295, 308, 308, 352, 308, 383, 339, 321};
int counter = 0;



int cycle_length2 (int n) {
    assert(n > 0);
    int c = 1;
    while (n > 1) {
        if ((n % 2) == 0) {
            n = (n >> 1);
        }
        else {
            n = (n + (n >> 1) + 1);
            ++c;
        }
        ++c;
        //
    }
    assert(c > 0);
    if(cache[n] == 0) {
        cache[n] = c;
    }
    return c;
}


// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (istream_iterator<int>& begin_iterator) {
    int i = *begin_iterator;
    ++begin_iterator;
    int j = *begin_iterator;
    ++begin_iterator;
    return make_pair(i, j);
}

// ------------
// collatz_eval
// ------------

tuple<int, int, int> collatz_eval (const pair<int, int>& p) {
    int i;
    int j;
    tie(i, j) = p;
    if (i > j)
    {
        int temp = i;
        i = j;
        j = temp;
    }
    int val = 0;
    int ret = 0;
    int half = (j>>1) +1;
    int start = 0;

    if (i < half)
    {
        start = half;
    }
    else
    {
        start = i;
    }

    for (int index = start; index <= j; index++) {
        if(cache[index]==0) {
            ret = cycle_length2(index);
            if(ret > val)
            {
                val = ret;
            }
        }
        else {
            if(cache[index] > val) {
                val = cache[index];
            }
        }
    }
    return make_tuple(i, j, val);

}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& sout, const tuple<int, int, int>& t) {
    int i;
    int j;
    int v;
    //cout << v << endl;
    tie(i, j, v) = t;
    //    cout << v << endl;
    sout << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& sin, ostream& sout) {
    istream_iterator<int> begin_iterator(sin);
    istream_iterator<int> end_iterator;
    while (begin_iterator != end_iterator) {
        collatz_print(sout, collatz_eval(collatz_read(begin_iterator)));
    }
}
